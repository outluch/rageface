// https://v3.nuxtjs.org/api/configuration/nuxt.config

export default defineNuxtConfig({
  // ssr: false,

  runtimeConfig: {
    public: {
      api: 'https://rageface.ru',
    },
  },

  modules: ['@nuxtjs/tailwindcss'],

  app: {
    head: {
      link: [
        { rel: 'icon', href: '/favicon.ico' },
        { rel: 'icon', sizes: '16x16', href: '/favicon-16x16.png' },
        { rel: 'icon', sizes: '32x32', href: '/favicon-32x32.png' },
        {
          rel: 'apple-touch-icon',
          sizes: '32x32',
          href: '/apple-touch-icon.png',
        },
        { rel: 'manifest', sizes: '32x32', href: '/site.webmanifest' },
      ],
      script: [
        {
          src: 'https://stats.outluch.com/js/script.js',
          'data-domain': 'rageface.ru',
          defer: true,
        },
      ],
    },
  },
})
